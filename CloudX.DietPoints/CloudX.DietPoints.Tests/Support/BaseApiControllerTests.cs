﻿using CloudX.DietPoints.Domain;
using CloudX.DietPoints.Domain.Model;
using CloudX.DietPoints.Web.Support;
using Moq;
using System.Security.Principal;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using CloudX.DietPoints.Web;
using CloudX.DietPoints.Domain.Security;
using System.Net.Http;
using System;
using Microsoft.Owin;

namespace CloudX.DietPoints.Tests.Support
{
    public class BaseApiControllerTests<TController> where TController : BaseApiController, new()
    {
        private IOwinContext owinContext;
        protected ModelDbContext Context { get; private set; }
        protected TController Controller { get; private set; }
        protected int DessertId { get; private set; }
        protected int SteakId { get; private set; }
        protected int SaladId { get; private set; }
        protected string LoggedUserId { get; private set; }

        public virtual void TestInitialize()
        {
            Context = ModelDbContext.Create();

            CreateTestData();
            CreateController();
        }

        public virtual void TestCleanup()
        {
            Context.Dispose();
            owinContext.Set<ModelDbContext>(null);
        }

        private void CreateController()
        { 
            Controller = new TController()
            {
                User = MockUser()
            };

            Controller.Request = new HttpRequestMessage(HttpMethod.Post,
                new Uri("test", UriKind.Relative)
                );

            owinContext = new OwinContext();
            Controller.Request.SetOwinContext(owinContext);
            owinContext.Set(Context);
        }

        private void CreateTestData()
        {
            ClearData();

            CreateUsers();

            var dessert = Context.FoodTypes.Add(new FoodType
            {
                CaloriesMultiplier = 1.5,
                Name = "Dessert",
                FoodGroup = FoodGroup.Dairy
            });

            var steak = Context.FoodTypes.Add(new FoodType
            {
                CaloriesMultiplier = 1.8,
                Name = "Steak",
                FoodGroup = FoodGroup.Dairy
            });

            var salad = Context.FoodTypes.Add(new FoodType
            {
                CaloriesMultiplier = 0.5,
                Name = "Salad",
                FoodGroup = FoodGroup.Vegetables
            });

            Context.SaveChanges();

            DessertId = dessert.Id;
            SteakId = steak.Id;
            SaladId = salad.Id;
        }

        private void CreateUsers()
        {
            var options = new IdentityFactoryOptions<ApplicationUserManager>();

            var userManager = ApplicationUserManagerConfigurator.Create(options, Context);

            var r = userManager.Register("ivo@test.com", "Password").Result;

            LoggedUserId = r.User.Id;

            r = userManager.Register("bowie@test.com", "bowiebowie").Result;
        }

        private void ClearData()
        {
            Context.Database.ExecuteSqlCommand("DELETE FROM Entries");
            Context.Database.ExecuteSqlCommand("DELETE FROM FoodTypes");
            Context.Database.ExecuteSqlCommand("DELETE FROM AspNetUsers");
        }

        private IPrincipal MockUser()
        {
            var userMock = new Mock<IPrincipal>();
            var identityMock = new Mock<ClaimsIdentity>();
            identityMock.Setup(x => x.FindFirst(It.IsAny<string>())).Returns(new Claim("test", LoggedUserId));
            userMock.Setup(x => x.Identity).Returns(identityMock.Object);

            return userMock.Object;
        }
    }
}