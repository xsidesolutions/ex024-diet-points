﻿namespace CloudX.DietPoints.Domain.Model
{
    public class FoodType
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual double CaloriesMultiplier { get; set; }
        public virtual FoodGroup FoodGroup { get; set; }
        public virtual byte[] Picture { get; set; }
        public virtual int CalculateDietPoints(Entry entry)
        {
            return (int)(entry.Calories * CaloriesMultiplier);
        }
    }
}
