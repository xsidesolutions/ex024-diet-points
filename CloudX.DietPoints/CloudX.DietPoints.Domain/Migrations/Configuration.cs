using System;
using System.Data.Entity.Migrations;
using CloudX.DietPoints.Domain.Model;
using System.Linq;

namespace CloudX.DietPoints.Domain.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<ModelDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private void SaveFoodType(ModelDbContext context, FoodType foodType)
        {
            if(!context.FoodTypes.Any(x=>x.Name == foodType.Name))
            {
                context.FoodTypes.Add(foodType);
            }
        }

        protected override void Seed(ModelDbContext context)
        {
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Meat,
                Name = "Steak",
                CaloriesMultiplier = 1.4,
                Picture = CreatePicture()
            });
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Meat,
                Name = "Chicken",
                CaloriesMultiplier = 1.2,
                Picture = CreatePicture()
            });
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Vegetables,
                Name = "Salad",
                CaloriesMultiplier = 0.8,
                Picture = CreatePicture()
            });
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Dairy,
                Name = "Eggs",
                CaloriesMultiplier = 1.1,
                Picture = CreatePicture()
            });
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Dairy,
                Name = "Cheese",
                CaloriesMultiplier = 1.2,
                Picture = CreatePicture()
            });
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Grain,
                Name = "Pizza",
                CaloriesMultiplier = 2,
                Picture = CreatePicture()
            });
            SaveFoodType(context, new FoodType
            {
                FoodGroup = FoodGroup.Fruit,
                Name = "Apple",
                CaloriesMultiplier = 0.8,
                Picture = CreatePicture()
            });
            context.SaveChanges();
        }

        private byte[] CreatePicture()
        {
            return new byte[10 * 1000 * 1000];
        }
    }
}