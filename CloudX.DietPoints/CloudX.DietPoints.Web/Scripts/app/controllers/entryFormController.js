﻿app.controller("entryFormController", function ($scope, $uibModalInstance, entry, entriesService, foodTypesService) {
    $scope.entry = Object.assign({}, entry);

    foodTypesService.getAll()
        .success(function (foodTypes) {
            $scope.foodTypes = foodTypes;
        })
        .error(function (response) {
            $scope.errors = parseErrors(response);
        });

    $scope.foodTypes = 
    $scope.errors = [];

    $scope.save = function () {
        entriesService.save($scope.entry)
                      .success(function () {
                          $uibModalInstance.close();
                      })
                      .error(function (response) {
                          $scope.errors = parseErrors(response);
                      });
    };

    $scope.close = function () {
        $uibModalInstance.dismiss("cancel");
    };
});