﻿app.controller("dietPointsController", function ($scope, $uibModal, $filter, confirmDialog, dietPointsService, entriesService) {
    clearFilterAndSearch();
    refreshDietPoints();

    $scope.search = search;

    $scope.pageChanged = function () {
        getEntries();
    };

    $scope.deleteEntry = function (entry) {
        var options = {
            message: "Are you sure?",
            title: "Delete Entry",
            confirm: function () {
                entriesService.delete(entry.id).success(function () {
                    getEntries();
                    refreshDietPoints();
                });
            }
        };
        confirmDialog(options);
    };

    $scope.clearFilterAndSearch = clearFilterAndSearch;

    $scope.filterWhen = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "/scripts/app/views/entriesFilter.html",
            controller: "entriesFilterController",
            resolve: {
                filter: function () { return $scope.filter }
            },
            windowClass: "filter-when"
        });

        modalInstance.result.then(function (filter) {
            $scope.filter = filter;
            getEntries();
        });
    }

    $scope.addNewEntry = function () {
        openEntryForm({ id: 0, date: new Date() });
    };

    $scope.editEntry = function (entry) {
        openEntryForm(entry);
    };

    $scope.configureExpectedDietPointsPerDay = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "/scripts/app/views/configureDietPointsPerDay.html",
            controller: "configureDietPointsPerDayController",
            resolve: {
                dietPoints: function () { return $scope.dietPoints.expectedDailyDietPoints }
            },
            windowClass: "expected-daily-dietPoints-configuration"
        });

        modalInstance.result.then(function (dietPoints) {
            dietPointsService.setDietPointsConfiguration({
                expectedDailyDietPoints: dietPoints
            }).success(refreshDietPoints);
        });
    };

    function getEntries() {
        entriesService.getEntries({
            page: $scope.currentPage,
            dateFrom: $scope.filter.dateFrom,
            dateTo: $scope.filter.dateTo,
            timeFrom: $scope.filter.timeFrom,
            timeTo: $scope.filter.timeTo
        }).success(function (response) {
            $scope.totalRecordsCount = response.totalRecords;
            $scope.entries = response.items;
        });
    };

    function search() {
        $scope.currentPage = 1;
        getEntries();
    };

    function openEntryForm(entry) {
        var modalInstance = $uibModal.open({
            templateUrl: "/scripts/app/views/entryForm.html",
            controller: "entryFormController",
            resolve: {
                entry: function () { return entry }
            }
        });

        modalInstance.result.then(function () {
            getEntries();
            refreshDietPoints();
        });
    }

    function refreshDietPoints() {
        dietPointsService.getDietPointsInfo()
            .success(function (response) {
                $scope.dietPoints = response;
            });
    }

    function clearFilterAndSearch() {
        $scope.filter = {
            dateFrom: null,
            dateTo: null,
            timeFrom: null,
            timeTo: null
        };
        search();
    }
});